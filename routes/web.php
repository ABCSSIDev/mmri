<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'administration/'], function(){
    Route::get('setting','Administration\SettingController@index')->name('setting');
    Route::resource('user','Administration\UserController');
});

Route::group(['prefix'=>'hris/'], function(){
    Route::resource('employee','EmployeeController@index');
});

Route::group(['prefix'=>'payroll/'], function(){
    
});

Route::group(['prefix'=>'inventory/'], function(){

});

