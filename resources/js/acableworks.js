//for sidebar script
require('./side-bar');

//for common script
require('./common');

//for jquery validator script
require('./jquery-validation');
