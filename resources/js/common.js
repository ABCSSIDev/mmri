/**
 * function to load print form to designated html file
 */
$(document).on('click','.print-form',function (e) {
    e.preventDefault();
    let form_id = $(this).attr('data-form');
    $('#'+form_id).removeAttr('hidden');
    printJS({
        printable: form_id,
        type: 'html',
        showModal: true,
        scanStyles:false,
        css: '/css/print.css'
    })
    $('#'+form_id).attr('hidden', true);
    return false;
});

/**
 * function to load for common scripts
 */
$(function () {
    //function to load bootstrap datetimepicker
    $('.datepicker').datetimepicker({
        format: 'L'
    });
    $('.timepicker').datetimepicker({
        format: 'LT'
    });

    //function to load jquery.number mask
    $(".amount").number(true,2);

    //script to load bootstrap selectpicker
    $('.selectpicker').selectpicker();

});

//function for uploading image
function onImage() {
    $('#image').click();
}
//function for uploading image
function onFileSelected() {
    var selectedFile = event.target.files[0];
    var files = event.target.files;
    var reader = new FileReader();
    if(selectedFile && selectedFile.size < 2097152)
    {
        var imgtag = document.getElementById("im");
        imgtag.title = selectedFile.name;
        reader.onload =  function(event) {
            imageIsLoaded(event);
            imgtag.src = event.target.result;
            
        };
        reader.readAsDataURL(selectedFile);
        
    }
}
//function for uploading image
function imageIsLoaded(e) {
    picture = e.target.result;
}

//function for call for modal layout
$(document).on('click','.popup_form',function(e){
    e.preventDefault();
    var title = $(this).attr('title');
    var url = $(this).attr('data-url');
    $('.modal-title').html(title);
    $.ajax({
        method: "GET",
        url: url,
        success: function(data){
            $('#myModal').modal({backdrop:'static'});
            $('#myModal').modal('show');
            $('div.modal-body').html(data);
        }
    });
});
