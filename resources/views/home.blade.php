@extends('layouts.app')

@section('content')
<div id="overlay" class="overlay"></div>
<div class="container-fluid p-5">
    <div class="row">
        <div class="form-group col-md-12">
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Accounts</li>
        </ol>
        </nav>
        <h4 class="form-head">Accounts List</h4>
        <div class="row justify-content-center">
            <div class="col-md-12 margin-top">
                <div align="right" style="margin: 0px 0px 20px 0px"><a href="{{ route('user.create') }}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> CREATE</button></a></div>
                <table class="cell-border" style="width:100%" id="account_list">
                    <thead >
                        <tr align="center">
                            <th>Image</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Contact No.</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody >

                    </tbody>
                </table>
                <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <a class="popup_form" data-toggle="modal" data-url="{{ route('user.show','1') }}" title="Sample Modal"> Sample Modal</a>
                        </div>
                    </div>
                <div class="form-group" align="right" >
                    <button type="button" class="btn btn-primary print-form" data-form="sample-print"><span class="fa fa-print"></span>&nbsp;Print</button>
                </div>
                
            </div>
        </div>
        </div>
    </div>
</div>
<div id="sample-print" hidden>
    <h1 class="sample">Sample Print with media print scss</h1>
</div>
@push('scripts')
    <script>
    $(document).ready(function(){
        $('#account_list').DataTable();
        $('.dataTables_empty').prepend('<img id="theImg" src="" />')
    });

    
    </script>
@endpush
@endsection
