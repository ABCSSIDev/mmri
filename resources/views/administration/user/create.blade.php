@extends('layouts.app')

@section('content')
<div id="overlay" class="overlay"></div>
<div class="container-fluid p-5">
    <div class="row">
        <div class="form-group col-md-12">
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item" >Setup</li>
            <li class="breadcrumb-item active" aria-current="page">Accounts</li>
        </ol>
        </nav>
        <h4 class="form-head">Accounts List</h4>
        <div class="row justify-content-center">
            <div class="col-md-12 ">
                <form method="POST" id="user-form" class="form-horizontal" enctype="multipart/form-data" action="{{ route('register') }}">
                    @csrf
                    <div class="errors"></div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Datepicker') }}</label>

                        <div class="col-md-6">
                            <input id="datepicker" type="text" class="form-control datepicker" name="datetimepicker" required >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="timepicker" class="col-md-4 col-form-label text-md-right">{{ __('Timepicker') }}</label>

                        <div class="col-md-6">
                            <input id="timepicker" type="text" class="form-control timepicker" name="datetimepicker" required >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Amount') }}</label>

                        <div class="col-md-6">
                            <input id="amount" type="text" class="form-control amount" name="amount" required >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Autocomplete') }}</label>

                        <div class="col-md-6">
                            <div class="ui-widget">
                                <input id="tags" type="text" class="form-control " name="" required >
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                    
                </form>              
            </div>
        </div>
        </div>
    </div>
    
</div>
@push('scripts')
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css'>
    <script src="{{ asset('js/jquery-ui.js') }}" ></script>
    <script src="{{ asset('js/jquery-autocomplete.js') }}"></script>
    
    <script>
        let rules = {
            name : {
                required: true
            },
            email : {
                required: true
            },
            password : {
                required: true
            },
            password_confirmation : {
                required: true,
                equalTo : "#password"
            },
        };
        $('#user-form').registerFields(rules);

        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];

        $( "#tags" ).autocomplete({
            source: availableTags,
            delay: 0,
            clearButton: true
        });
    </script>
@endpush
@endsection
