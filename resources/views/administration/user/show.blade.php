<form method="POST" action="" class="form-horizontal" enctype="multipart/form-data">
{{ csrf_field() }}
<input type="hidden" name="_method" value="DELETE" >
    <h6>Sample modal form</h6>
    <br>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>